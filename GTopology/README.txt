1. To install in userspace run:

$ python setup.py install --user

or to setup in global space

$ sudo setup.py install

2. Run tests:

$ cd gtopology; python topology_test.py; cd ..

You must see "OK" after test execution

3. Run simple example from python CLI interface:

$ cd examples; python gen_diag.py '{1:2, 3:4}'; cd ..
e12|e3|33||
e12|23|3|e|


